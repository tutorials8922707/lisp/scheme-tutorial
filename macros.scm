#lang scheme

(define-syntax
  3-state
  (syntax-rules ()
    ((3-state
      value positive-body zero-body negative-body)
     (cond
       ((zero? value) zero-body)
       ((positive? value) positive-body)
       (else negative-body)))))

(3-state 100 (display "P") (display "Z") (display "N"))
(3-state 0 (display "P") (display "Z") (display "N"))
(3-state -100 (display "P") (display "Z") (display "N"))

(define-syntax for
  (syntax-rules (in as)
    ((for element in list
       body ...)
     (for-each (lambda (element)
                 body ...)
               list))
    ((for list as element
       body ...)
     (for element in list
       body ...))))
(let ((names '(Alice Bob Eve)))
  (for name in names
    (display "Hello ")
    (display name)
    (newline))
  (for names as name
    (display "name: ")
    (display name)
    (newline)))

(newline)

(define-syntax forn
  (syntax-rules (in as)
    ((forn element in list
       body)
     (for-each (lambda (element)
                 body)
               list))
    ((forn list as element
       body)
     (for element in list
       body))))

(let ((names '(Alice Bob Eve)))
  (forn name in names
    (display name))
  (forn names as name
    (display name)))

(newline)
; Expands to
(for-each (lambda (name) (display name)) '(Alice Bob Eve))