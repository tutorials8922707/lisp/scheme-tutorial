#lang scheme

"Sum the elements of a list"
(define sumlist
  (lambda (lst)
    (if (null? lst)
        0
        (+ (car lst) (sumlist (cdr lst))))))

(sumlist '(1 2 3 4))

(newline)
"Double each element of the list"
(define double
  (lambda (x)
    (* x 2)))
(define doublelist
  (lambda (lst)
    (if (null? lst)
        '()
        (cons (double (car lst)) (doublelist (cdr lst))))))

(doublelist '(1 2 3 4))

(newline)
"The sum of the double of each element in the list"
(define sumdoublelist
  (lambda (lst)
    (if (null? lst)
        0
        (+ (* 2 (car lst)) (sumdoublelist (cdr lst))))))

(sumdoublelist '(1 2 3 4))

(newline)
"Summing the doubles fo a list using methids ew've already written"
(define better-sumdoublelist
  (lambda (lst)
    (sumlist (doublelist lst))))
(better-sumdoublelist '(1 2 3 4))

(newline)
"Add two lists together (pairwise)"
(define myaddlists
  (lambda (lst1 lst2)
    (cond
      ((null? lst1) lst2)
      ((null? lst2) lst1)
      (else (cons (+ (car lst1) (car lst2)) (myaddlists (cdr lst1) (cdr lst2)))))))
(myaddlists '(1 2 3 4) '(2 3 4 5))
(myaddlists '(1 2 3 4) '(2 3 4))
(myaddlists '(1 2 3) '(2 3 4 5))

(newline)
(define numlist
  (lambda (x)
    (if (< x 1)
        '()
        (append (numlist (- x 1)) (list x)))))
(numlist 4)