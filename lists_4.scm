#lang scheme
(define my-gen-number-list
  (lambda (min max)
    (if (= min max)
        (list max)
        (cons min (my-gen-number-list (+ 1 min) max)))))
(my-gen-number-list 2 9)

(newline)
(define gen-number-list
  (lambda (start end)
    (if (>= start end)
        (list start)
        (cons start (gen-number-list (+ start 1) end)))))
(gen-number-list 2 9)

(newline)
(define my-triple-all
  (lambda (lst)
    (if (null? lst)
        '()
        (cons (* 3 (car lst)) (my-triple-all (cdr lst))))))
(my-triple-all '(1 2 3 4 5))

(newline)
"triple-list"
(define triple-list
  (lambda (lst)
    (if (null? lst)
        '()
        (cons (* (car lst) 3) (triple-list (cdr lst))))))
(triple-list '(1 2 3))
(newline)
(define letter-grade
  (lambda (grade)
    (cond
      ((>= grade 900) 'A)
      ((>= grade 800) 'B)
      ((>= grade 700) 'C)
      ((>= grade 600) 'D)
      (else 'F))))
"grade->letter"
(define grade->letter
  (lambda (gradelst)
    (if (null? gradelst)
        '()
        (cons (letter-grade (car gradelst)) (grade->letter (cdr gradelst))))))